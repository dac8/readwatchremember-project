﻿using AutoMapper;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Requests;
using ReadWatchRememberApp.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MyBookEntity, MyBookDetailsResponse>()
                .ForMember(p => p.Stage, t => t.MapFrom(source => source.Stage.ToString()));
            //.ForMember(p=>p.BookTitle,t=>t.MapFrom(source=>source.Book.Title))
            //.ForMember(p => p.BookAuthor, t => t.MapFrom(source => source.Book.Author));

            CreateMap<MyMovieEntity, MyMovieDetailsResponse>()
                .ForMember(p => p.Stage, t => t.MapFrom(source => source.Stage.ToString()));

            CreateMap<MySeriesEntity, MySeriesDetailsResponse>()
                .ForMember(p => p.Stage, t => t.MapFrom(source => source.Stage.ToString()));

            CreateMap<MyBookUpdateRequest, MyBookEntity>()
              .ForAllMembers(p => p.Condition((q, s, m) => m != null));

            CreateMap<MyMovieSeriesUpdateRequest, MyMovieEntity>()
             .ForAllMembers(p => p.Condition((q, s, m) => m != null));

            CreateMap<MyMovieSeriesUpdateRequest, MySeriesEntity>()
             .ForAllMembers(p => p.Condition((q, s, m) => m != null));

            CreateMap<SeriesUpdateRequest, SeriesEntity>() 
             .ForAllMembers(p => p.Condition((q, s, m) => m != null));
        }
    }
}
