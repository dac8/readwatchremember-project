﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.HostedServices
{
    public class SeedDatabaseHostedService : IHostedService, IDisposable
    {
        private Timer _timer;
        private IServiceScopeFactory Services { get; }

        public SeedDatabaseHostedService(IServiceScopeFactory services)
        {
            Services = services;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                _timer = new Timer(async _ => await SeedDatabase(),
                    null, TimeSpan.Zero, TimeSpan.FromSeconds(7));
            }
            catch
            {

            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return null;
        }

        public void Dispose()
        {
        }

        private async Task SeedDatabase()
        {
            using var scope = Services.CreateScope();
            await SeedRoles(scope);
        }

        private async Task SeedRoles(IServiceScope scope)
        {
            using var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<RoleEntity>>();
            var rolesList = Enum.GetNames<EnumRoles>().ToList();

            foreach (var role in rolesList)
            {
                if (!await roleManager.RoleExistsAsync(role))
                {
                    var result = await roleManager.CreateAsync(new RoleEntity
                    {
                        Name = role
                    });

                }
            }
        }
    }
}
