﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class MySeriesRepository : BaseRepository<MySeriesEntity>
    {
        public MySeriesRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<MySeriesEntity>> GetAll(int listId)
        {
            return await DbContext.UsersSeries.Where(p => p.MyListId == listId)
                //.Include(p => p.Series)
                .ToListAsync();
        }

        public async Task<List<MySeriesEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Series.Title.Contains(text))
                .ToListAsync();
        }
    }
}

