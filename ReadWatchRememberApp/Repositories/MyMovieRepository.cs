﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class MyMovieRepository : BaseRepository<MyMovieEntity>
    {
        public MyMovieRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<MyMovieEntity>> GetAll(int listId)
        {
            return await DbContext.UsersMovies.Where(p=>p.MyListId==listId)
                //.Include(p => p.Movie)
                .ToListAsync();
        }


        public async Task<List<MyMovieEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Movie.Title.Contains(text))
                .ToListAsync();
        }
    }
}
