﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class MovieRepository : BaseRepository<MovieEntity>
    {
        public MovieRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<MovieEntity>> GetAll()
        {
            return await DbContext.Movies.ToListAsync();
        }


        public async Task<List<MovieEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Title.Contains(text))
                .ToListAsync();
        }
    }
}

