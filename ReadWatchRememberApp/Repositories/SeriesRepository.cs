﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class SeriesRepository :BaseRepository<SeriesEntity>
    {
        private readonly ReadWatchRememberDbContext _dbContext;

        public SeriesRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<SeriesEntity>> GetAll()
        {
            return await _dbContext.Series.ToListAsync();
        }

        public async Task<SeriesEntity> GetById(int id)
        {
            var result = await _dbContext.Series.FirstOrDefaultAsync(p => p.Id == id);
            return result;
        }

        public async Task<SeriesEntity> Update(SeriesEntity series)
        {
            var result = _dbContext.Series.Update(series);
            await _dbContext.SaveChangesAsync();
            return result.Entity;

        }

        public async Task<bool> Delete(int id)
        {
            var series = await GetById(id);

            if (series != null)
            {
                _dbContext.Series.Remove(series);
                await _dbContext.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<SeriesEntity> Insert(SeriesEntity series)
        {
            EntityEntry<SeriesEntity> result = await _dbContext.Series.AddAsync(series);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<List<SeriesEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Title.Contains(text))
                .ToListAsync();
        }
    }
}
