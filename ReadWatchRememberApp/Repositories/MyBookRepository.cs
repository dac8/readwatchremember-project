﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class MyBookRepository : BaseRepository<MyBookEntity>
    {
        public MyBookRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<MyBookEntity>> GetAll(int listId)
        {
            return await DbContext.UsersBook.Where(p=>p.MyListId==listId)
                //.Include(p => p.Book)
                  .ToListAsync();
        }

        public async Task<List<MyBookEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Book.Title.Contains(text))
                .ToListAsync();
        }
    }
}
