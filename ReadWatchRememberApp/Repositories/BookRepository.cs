﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class BookRepository : BaseRepository<BookEntity>
    {
        public BookRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<BookEntity>> GetAll()
        {
            return await DbContext.Books.ToListAsync();
        }

        public async Task<List<BookEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Title.Contains(text))
                .ToListAsync();
        }
    }
}

