﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class BookClubRepository : BaseRepository<BookClubEntity>
    {
        public BookClubRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<BookClubEntity>> GetAll()
        {
            return await DbContext.BookClubs.Include(p => p.UserBookClubs).ThenInclude(p=>p.User).ToListAsync();
        }

        public async Task<List<BookClubEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Name.Contains(text))
                .ToListAsync();
        }
    }
}
