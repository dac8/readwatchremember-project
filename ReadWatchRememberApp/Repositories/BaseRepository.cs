﻿using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class BaseRepository<T> where T : BaseEntity
    {
        protected readonly ReadWatchRememberDbContext DbContext;
        protected readonly DbSet<T> Table;

        public BaseRepository(ReadWatchRememberDbContext dbContext)
        {
            DbContext = dbContext;
            Table = DbContext.Set<T>();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate = null)
        {
            if (predicate != null)
            {
                return Table.Where(predicate);
            }

            return Table;
        }

        public async Task<int> CountAll(Expression<Func<T, bool>> predicate = null)
        {
            if (predicate != null)
                return await Table
                    .Where(predicate)
                    .CountAsync();

            return await Table.CountAsync();
        }

        public async Task Commit()
        {
            await DbContext.SaveChangesAsync();
        }

        public async Task<T> Create(T entity, bool commit = true)
        {
            await Table.AddAsync(entity);

            if (commit)
                await Commit();

            return entity;
        }

        public async Task<T> Update(T entity, bool commit = true)
        {
            Table.Update(entity);

            if (commit)
                await Commit();

            return entity;
        }

        public async Task<T> Delete(T entity, bool commit = true)
        {
            entity.IsDeleted = true;
            Table.Update(entity);
            //Table.Remove(entity);

            if (commit)
                await Commit();

            return entity;
        }
    }
}
