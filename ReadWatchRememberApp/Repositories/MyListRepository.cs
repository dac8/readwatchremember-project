﻿using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Repositories
{
    public class MyListRepository : BaseRepository<MyListEntity>
    {
        public MyListRepository(ReadWatchRememberDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<MyListEntity>> GetAll()
        {
            return await DbContext.UsersMyList.ToListAsync();
        }

        public async Task<MyListEntity> GetUserList(int userId)
        {
            var result = DbContext.UsersMyList.ToList();
            return await DbContext.UsersMyList.Where(u => u.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task<List<MyListEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.User.UserName.Contains(text))
                .ToListAsync();
        }
    }
}
