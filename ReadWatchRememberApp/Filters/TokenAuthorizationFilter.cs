﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using ReadWatchRememberApp.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace ReadWatchRememberApp.Filters
{
    public class TokenAuthorizationFilter : IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            string token = context.HttpContext.Request.Headers["Authorization"];
            token = token?.Replace("Bearer", "").Trim();

            if (token != null)
            {
                var tokenHandler = (JwtSecurityToken)new JwtSecurityTokenHandler().ReadToken(token);
                using var claims = tokenHandler.Claims.GetEnumerator();

                while (claims.MoveNext())
                {
                    if (claims.Current?.Type == ClaimTypes.NameIdentifier)
                    {
                        var userId = claims.Current.Value;
                        var userService = context.HttpContext.RequestServices.GetService<UserService>();

                        var dbUser = await userService
                            .Get(p => p.Id.ToString() == userId)
                            .FirstOrDefaultAsync();

                        if (dbUser?.IsActive == false)
                            throw new BadHttpRequestException("User not active");
                    }
                }
            }
        }
    }
}
