﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using ILogger = NLog.ILogger;
using SendGrid.Helpers.Errors.Model;
using System.Net;
using Newtonsoft.Json;
using ReadWatchRememberApp.Models.Exceptions;

namespace ReadWatchRememberApp.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value!.Contains("/api/"))
            {
                try
                {
                    await _next(context);
                }
                catch (NotFoundException ex)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    var response = JsonConvert.SerializeObject(new ExceptionResponse
                    {
                        Message = ex.Message,
                        Trace = ex.StackTrace,
                        Type = ex.GetType().Name
                    });

                    await context.Response.WriteAsync(response);
                    _logger.Info(response);
                    _logger.Error(ex, "Not found exception");
                }
                catch (CustomException ex)
                {
                    context.Response.StatusCode = (int)ex.Status;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(ex.Value));
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new ExceptionResponse
                    {
                        Message = ex.Message,
                        Trace = ex.StackTrace,
                        Type = ex.GetType().Name
                    }));
                }
            }
            else
            {
                await _next(context);
            }
        }
    }
}
