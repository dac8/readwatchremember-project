﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class MyListService : BaseService<MyListEntity>
    {
        private readonly MyListRepository _myListRepository;

        public MyListService(MyListRepository myListRepository, 
            IHttpContextAccessor contextAccessor)
            : base(myListRepository, contextAccessor)
        {
            _myListRepository = myListRepository;
        }

        public async Task<List<MyListEntity>> GetAll()
        {
            return await _myListRepository.GetAll();
        }

        public async Task<MyListEntity> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                .Include(p=>p.User)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.ListNotFound);

            return result;
        }

        public async Task<MyListEntity> GetUserList(int userId)
        {
            return await _myListRepository.GetUserList(userId);
        }

        public async Task<List<MyListEntity>> Search(string text)
        {
            return await _myListRepository.Search(text);
        }
    }
}
