﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class BookClubService : BaseService<BookClubEntity>
    {
        private readonly BookClubRepository _entityRepository;

        public BookClubService(BookClubRepository entityRepository,
              IHttpContextAccessor contextAccessor)
            : base(entityRepository, contextAccessor)
        {
            _entityRepository = entityRepository;
        }

        public async Task<List<BookClubEntity>> GetAll()
        {
            return await _entityRepository.GetAll();
        }

        public async Task<BookClubEntity> GetById(int bookClubId)
        {
            var result = await Get(p => p.Id == bookClubId)
                .Include(p=>p.UserBookClubs)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.BookClubNotFound);

            return result;
        }

        public async Task<List<BookClubEntity>> Search(string text)
        {
            return await _entityRepository.Search(text);
        }
    }
}
