﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Responses;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class MySeriesService : BaseService<MySeriesEntity>
    {
        private readonly MySeriesRepository _listEntityRepository;
        private readonly IMapper _mapper;

        public MySeriesService(MySeriesRepository listEntityRepository,
            IHttpContextAccessor contextAccessor,
            IMapper mapper)
            : base(listEntityRepository, contextAccessor)
        {
            _listEntityRepository = listEntityRepository;
            _mapper = mapper;
        }

        public async Task<List<MySeriesDetailsResponse>> GetAllResponse(int listId)
        {
            var result = await Get(p => p.MyListId == listId)
                //.Include(p => p.Series)
                .ToListAsync();

            return _mapper.Map<List<MySeriesDetailsResponse>>(result);
        }

        public async Task<List<MySeriesEntity>> GetAll(int listId)
        {
            return await _listEntityRepository.GetAll(listId);
        }


        public async Task<MySeriesDetailsResponse> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.SeriesNotFound);

            return _mapper.Map<MySeriesDetailsResponse>(result);
        }
        public async Task<List<MySeriesEntity>> Search(string text)
        {
            return await _listEntityRepository.Search(text);
        }
    }
}

