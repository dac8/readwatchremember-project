﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Responses;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class MyBookService : BaseService<MyBookEntity>
    {
        private readonly MyBookRepository _entityRepository;
        private readonly IMapper _mapper;

        public MyBookService(MyBookRepository listEntityRepository,
            IHttpContextAccessor contextAccessor,
            IMapper mapper)
            : base(listEntityRepository, contextAccessor)
        {
            _entityRepository = listEntityRepository;
            _mapper = mapper;
        }

        public async Task<List<MyBookDetailsResponse>> GetAllResponse(int listId)
        {
            var result = await Get(p => p.MyListId == listId)
                //.Include(p => p.Book)
                .ToListAsync();

            return _mapper.Map<List<MyBookDetailsResponse>>(result);
        }

        public async Task<List<MyBookEntity>> GetAll(int listId)
        {
            return await _entityRepository.GetAll(listId);
        }

        public async Task<MyBookDetailsResponse> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                //.Include(p => p.Book)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.BookNotFound);

            return _mapper.Map<MyBookDetailsResponse>(result);
        }

        public async Task<List<MyBookEntity>> Search(string text)
        {
            return await _entityRepository.Search(text);
        }
    }
}
