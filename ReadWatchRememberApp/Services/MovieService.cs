﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class MovieService : BaseService<MovieEntity>
    {
        private readonly MovieRepository _entityRepository;

        public MovieService(MovieRepository entityRepository,
            IHttpContextAccessor contextAccessor)
            : base(entityRepository, contextAccessor)
        {
            _entityRepository = entityRepository;
        }

        public async Task<List<MovieEntity>> GetAll()
        {
            return await _entityRepository.GetAll();
        }

        public async Task<MovieEntity> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.MovieNotFound);

            return result;
        }

        public async Task<List<MovieEntity>> Search(string text)
        {
            return await _entityRepository.Search(text);
        }
    }
}
