﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Responses;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class MyMovieService : BaseService<MyMovieEntity>
    {
        private readonly MyMovieRepository _listEntityRepository;
        private readonly IMapper _mapper;

        public MyMovieService(MyMovieRepository listEntityRepository,
            IHttpContextAccessor contextAccessor,
            IMapper mapper)
            : base(listEntityRepository, contextAccessor)
        {
            _listEntityRepository = listEntityRepository;
            _mapper = mapper;
        }

        public async Task<List<MyMovieDetailsResponse>> GetAllResponse(int listId)
        {
            var result = await Get(p => p.MyListId == listId)
                //.Include(p => p.Movie)
                .ToListAsync();

            return _mapper.Map<List<MyMovieDetailsResponse>>(result);
        }

        public async Task<List<MyMovieEntity>> GetAll(int listId)
        {
            return await _listEntityRepository.GetAll(listId);
        }

        public async Task<MyMovieDetailsResponse> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                //.Include(p=>p.Movie)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.MovieNotFound);

            return _mapper.Map<MyMovieDetailsResponse>(result);
        }

        public async Task<List<MyMovieEntity>> Search(string text)
        {
            return await _listEntityRepository.Search(text);
        }
    }
}
