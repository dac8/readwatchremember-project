﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Requests;
using ReadWatchRememberApp.Models.Responses;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class UserService
    {
        private readonly UserRepository _userRepository;
        private readonly EmailService _emailService;
        private readonly MyListService _myListService;
        private readonly string _tokenKey = @"dfghjkl-dfghbjkl-krgnjkwgjkbwrkjgbwkgbfjkerglkjhgvcgfxdghjk";

        public UserService(UserRepository userRepository,
            MyListService myListService,
            EmailService emailService)
        {
            _userRepository = userRepository;
            _emailService = emailService;
            _myListService = myListService;
        }

        public IQueryable<UserEntity> Get(Expression<Func<UserEntity, bool>> predicate = null)
        {
            return _userRepository.Get(predicate);
        }

        public async Task<UserEntity> GetById(int userId)
        {
            return await _userRepository.GetById(userId);
        }

        public async Task<UserEntity> GetUserDetails(int userId)
        {
            return await Get(p => p.Id == userId)
                .Include(p => p.UserRoles)
                .ThenInclude(p => p.Role)
                .FirstOrDefaultAsync();
        }

        public async Task<IdentityResult> RegisterUser(UserRegisterRequest userRequest, string role)
        {
            var user = new UserEntity
            {
                UserName = userRequest.Username,
                Email = userRequest.Email,
                IsActive = true,
                ConfirmationCode = new Random().Next(0, 1000000).ToString("D6"),
                ConfirmationCodeExpires = DateTime.Now.AddMinutes(2)
            };

            var result = await _userRepository.Register(user, userRequest.Password);

            if (!result.Succeeded)
                return result;

            var roleResult = await _userRepository.AddRoleToUser(user, role);

            if (!roleResult.Succeeded)
                return result;

            await _emailService.SendEmailConfirmation(user.Email, user.ConfirmationCode);

            var dbUser = await _userRepository.GetUserByUsername(user.UserName);
            
            await _myListService.Create(new MyListEntity() { UserId = dbUser.Id, User = dbUser, CreatedBy = dbUser.Id, ModifiedBy = dbUser.Id });

            return result;
        }

        public async Task<bool> ResendEmailConfirmation(string email)
        {
            var user = await Get(p => p.Email == email).FirstOrDefaultAsync();

            if (user == null)
                throw new NotFoundException(StringConstants.UserNotFound);

            if (user.EmailConfirmed)
                throw new BadRequestException("Already Confirmed");

            user.ConfirmationCode = new Random().Next(0, 1000000).ToString("D6");
            user.ConfirmationCodeExpires = DateTime.Now.AddMinutes(2);
            await _userRepository.UpdateUser(user);
            return await _emailService.SendEmailConfirmation(email, user.ConfirmationCode);
        }

        public async Task<bool> ConfirmEmail(string email, string code)
        {
            var user = await Get(p => p.Email == email).FirstOrDefaultAsync();

            if (user == null)
                throw new NotFoundException("User not found");

            if (user.EmailConfirmed)
                throw new BadRequestException("Already Confirmed");

            if (user.ConfirmationCode != code || user.ConfirmationCodeExpires < DateTime.Now)
                throw new BadRequestException("Invalid Code");

            var result = await _userRepository.ConfirmEmail(user);

            if (result.Succeeded)
                return true;

            return false;
        }

        public async Task<UserLogInResponse> Login(string username, string password)
        {
            var result = await _userRepository.Login(username, password);

            if (!result.Succeeded)
                return null;

            var dbUser = await _userRepository.GetUserByUsername(username);

            if (dbUser == null)
                return null;

            if (dbUser.EmailConfirmed == false)
                throw new BadRequestException("User not confirmed");

            dbUser.RefreshToken = GenerateRefreshToken();
            dbUser.RefreshTokenExpires = DateTime.Now.AddMinutes(60d);
            await _userRepository.UpdateUser(dbUser);

            return new UserLogInResponse
            {
                Token = GenerateToken(dbUser),
                RefreshToken = dbUser.RefreshToken,
                User = dbUser,
                Result = result
            };
        }

        public async Task<UserLogInResponse> RefreshToken(string refreshToken)
        {
            var dbUser = await _userRepository.GetUserByRefreshToken(refreshToken);

            if (dbUser?.RefreshTokenExpires == null ||
                dbUser.RefreshTokenExpires < DateTime.Now)
                return null;

            dbUser.RefreshToken = GenerateRefreshToken();
            dbUser.RefreshTokenExpires = DateTime.Now.AddMinutes(60d);
            await _userRepository.UpdateUser(dbUser);

            return new UserLogInResponse
            {
                Token = GenerateToken(dbUser),
                RefreshToken = dbUser.RefreshToken,
                User = dbUser
            };
        }

        public async Task<bool?> RevokeRefreshToken(string refreshToken)
        {
            var dbUser = await _userRepository.GetUserByRefreshToken(refreshToken);

            if (dbUser == null)
                return null;

            dbUser.RefreshTokenExpires = DateTime.Now;
            await _userRepository.UpdateUser(dbUser);
            return true;
        }

        private string GenerateToken(UserEntity user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Email, user.Email)
            };

            claims.AddRange(user.UserRoles.Select(p => p.Role.Name).Select(p => new Claim(ClaimTypes.Role, p)));

            var token = new JwtSecurityToken(
                "https://readwatchremember.ro",
                "https://readwatchremember.ro",
                claims,
                DateTime.Now,
                DateTime.Now.AddMinutes(30d),
                new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenKey)),
                    SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using var generator = new RNGCryptoServiceProvider();
            generator.GetBytes(randomNumber);

            return Convert.ToBase64String(randomNumber);
        }

        public async Task<UserEntity> Update(UserEntity user)
        {
            return await _userRepository.UpdateUser(user);
        }
    }
}
