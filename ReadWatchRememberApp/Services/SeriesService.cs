﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class SeriesService : BaseService<SeriesEntity>
    {
        private readonly SeriesRepository _entityRepository;

        public SeriesService(SeriesRepository entityRepository,
            IHttpContextAccessor contextAccessor)
            : base(entityRepository, contextAccessor)
        {
            _entityRepository = entityRepository;
        }

        public async Task<List<SeriesEntity>> GetAll()
        {
            return await _entityRepository.GetAll();
        }

        public async Task<SeriesEntity> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.SeriesNotFound);

            return result;
        }

        public async Task<List<SeriesEntity>> Search(string text)
        {
            return await _entityRepository.Search(text);
        }
    }
}
