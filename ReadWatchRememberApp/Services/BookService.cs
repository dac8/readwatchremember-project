﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Repositories;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Services
{
    public class BookService : BaseService<BookEntity>
    {
        private readonly BookRepository _entityRepository;

        public BookService(BookRepository entityRepository,
            IHttpContextAccessor contextAccessor)
            : base(entityRepository, contextAccessor)
        {
            _entityRepository = entityRepository;
        }

        public async Task<List<BookEntity>> GetAll()
        {
            return await _entityRepository.GetAll();
        }

        public async Task<BookEntity> GetById(int id)
        {
            var result = await Get(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (result == null)
                throw new NotFoundException(StringConstants.BookNotFound);

            return result;
        }

        public async Task<List<BookEntity>> Search(string text)
        {
            return await _entityRepository.Search(text);
        }
    }
}
