using System;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using ReadWatchRememberApp.Models.DataBase;
using ReadWatchRememberApp.Repositories;
using ReadWatchRememberApp.Services;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models;
using Newtonsoft.Json;
using ReadWatchRememberApp.HostedServices;
using ReadWatchRememberApp.Filters;
using ReadWatchRememberApp.Helpers;
using ReadWatchRememberApp.Middleware;
//using Serilog;
//using Serilog.Events;


namespace ReadWatchRememberApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddDbContext<ReadWatchRememberDbContext>(o => o.UseSqlServer(Configuration.GetConnectionString("ReadWatchRememberDb")));

            services.AddIdentity<UserEntity, RoleEntity>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;

                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;

                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(2d);
                options.Lockout.MaxFailedAccessAttempts = 3;
            })
                .AddEntityFrameworkStores<ReadWatchRememberDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthorization()
                .AddAuthentication(o =>
                {
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = "https://readwatchremember.ro",
                        ValidAudience = "https://readwatchremember.ro",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                            @"dfghjkl-dfghbjkl-krgnjkwgjkbwrkjgbwkgbfjkerglkjhgvcgfxdghjk")),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddScoped<MyMovieRepository>();
            services.AddScoped<MyMovieService>();

            services.AddScoped<MyBookRepository>();
            services.AddScoped<MyBookService>();

            services.AddScoped<MySeriesRepository>();
            services.AddScoped<MySeriesService>();

            services.AddScoped<BookRepository>();
            services.AddScoped<BookService>();

            services.AddScoped<MovieRepository>();
            services.AddScoped<MovieService>();

            services.AddScoped<SeriesRepository>();
            services.AddScoped<SeriesService>();

            services.AddScoped<EmailService>();

            services.AddScoped<MyListRepository>();
            services.AddScoped<MyListService>();

            services.AddScoped<UserRepository>();
            services.AddScoped<UserService>();

            services.AddScoped<BookClubRepository>();
            services.AddScoped<BookClubService>();



            services.AddSingleton(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
            services.AddHostedService<SeedDatabaseHostedService>();

            services.AddSingleton(new MapperConfiguration(p => p.AddProfile(new MappingProfile())).CreateMapper());

            services.AddControllers().AddNewtonsoftJson(x =>
                x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            services.AddMvcCore(o => o.Filters.Add(new TokenAuthorizationFilter()));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ReadWatchRememberApp", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ReadWatchRememberApp v1"));
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
