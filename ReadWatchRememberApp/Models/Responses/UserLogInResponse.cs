﻿using Microsoft.AspNetCore.Identity;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Responses
{
    public class UserLogInResponse
    {
        public SignInResult Result { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public UserEntity User { get; set; }
    }
}
