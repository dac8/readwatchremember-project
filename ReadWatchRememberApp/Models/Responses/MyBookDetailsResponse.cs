﻿using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Responses
{
    public class MyBookDetailsResponse : BaseResponse
    {
        public int BookId;
        public float Rating { get; set; }
        public string Stage { get; set; }
    }
}
