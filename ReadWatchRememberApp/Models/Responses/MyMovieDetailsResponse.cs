﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Responses
{
    public class MyMovieDetailsResponse : BaseResponse
    {
        public int MovieId { get; set; }
        public float Rating { get; set; }
        public string Stage { get; set; }
    }
}
