﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Responses
{
    public class MySeriesDetailsResponse : BaseResponse
    {
        public int SeriesId { get; set; }
        public float Rating { get; set; }
        public string Stage { get; set; }
    }
}
