﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("BookClub")]
    public class BookClubEntity : BaseEntity
    {
        public string Name { get; set; }
        public int NrMembers { get; set; }
        public List<UserBookClubEntity> UserBookClubs { get; set; }
    }
}
