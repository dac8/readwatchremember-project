﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("Movie")]
    public class MovieEntity:BaseEntity
    {
        public string Title { get; set; }
        public int ReleaseYear { get; set; }
    }
}
