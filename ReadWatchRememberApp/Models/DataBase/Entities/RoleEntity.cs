﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    public class RoleEntity : IdentityRole<int>
    {
        public List<UserRoleEntity> UserRoles { get; set; }
    }
}
