﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("MyList")]
    public class MyListEntity : BaseEntity
    {
        public int UserId { get; set; }
        [ForeignKey("UserId")] public UserEntity User { get; set; }
        public List<MyBookEntity> MyBooks { get; set; }
        public List<MyMovieEntity> MyMovies { get; set; }
        public List<MySeriesEntity> MySeries { get; set; }
    }
}
