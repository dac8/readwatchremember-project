﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("UserBookClub")]
    public class UserBookClubEntity
    {
        public int UserId { get; set; }
        [ForeignKey("UserId")] public UserEntity User { get; set; }

        public int BookClubId { get; set; }
        [ForeignKey("BookClubId")]  public BookClubEntity BookClub { get; set; }
    }
}
