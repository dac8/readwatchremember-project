﻿using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("MyBook")]
    public class MyBookEntity: BaseEntity
    {
        public int BookId { get; set; }
        [ForeignKey("BookId")] public BookEntity Book { get; set; }
        public int MyListId { get; set; }
        [ForeignKey("MyListId")] public MyListEntity MyList { get; set; }
        public float Rating { get; set; }
        public EnumBookStage Stage { get; set; }
    }
}
