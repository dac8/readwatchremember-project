﻿using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("Series")]
    public class SeriesEntity:BaseEntity
    {
        public string Title { get; set; }
        public int Seasons { get; set; }
        public int ReleaseYear { get; set; }
        public EnumSeriesStage Stage { get; set; }
    }
}
