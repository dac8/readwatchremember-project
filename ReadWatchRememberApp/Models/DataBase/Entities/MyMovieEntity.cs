﻿using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase.Entities
{
    [Table("MyMovie")]
    public class MyMovieEntity:BaseEntity
    {
        public int MovieId { get; set; }
        [ForeignKey("MovieId")] public MovieEntity Movie { get; set; }
        public int MyListId { get; set; }
        [ForeignKey("MyListId")] public MyListEntity MyList { get; set; }
        public float Rating { get; set; }
        public EnumMovieSeriesStage Stage { get; set; }
    }
}
