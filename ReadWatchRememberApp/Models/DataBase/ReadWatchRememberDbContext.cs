﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ReadWatchRememberApp.Models.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.DataBase
{
    public class ReadWatchRememberDbContext : IdentityDbContext<UserEntity, RoleEntity, int, IdentityUserClaim<int>, UserRoleEntity,
       IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public ReadWatchRememberDbContext(DbContextOptions<ReadWatchRememberDbContext> options) : base(options)
        {
        }

        public DbSet<MovieEntity> Movies { get; set; }
        public DbSet<MyMovieEntity> UsersMovies { get; set; }
        public DbSet<BookEntity> Books { get; set; }
        public DbSet<MyBookEntity> UsersBook { get; set; }
        public DbSet<SeriesEntity> Series { get; set; }
        public DbSet<MySeriesEntity> UsersSeries { get; set; }
        public DbSet<BookClubEntity> BookClubs { get; set; }
        public DbSet<MyListEntity> UsersMyList { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BookEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);

            modelBuilder.Entity<MovieEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);

            modelBuilder.Entity<SeriesEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);
            
            modelBuilder.Entity<MyBookEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);

            modelBuilder.Entity<MyMovieEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);

            modelBuilder.Entity<MySeriesEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);

            modelBuilder.Entity<BookClubEntity>()
                .HasQueryFilter(p => p.IsDeleted == false);

            modelBuilder.Entity<UserEntity>()
                .HasMany(e => e.UserRoles)
                .WithOne(e => e.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            modelBuilder.Entity<RoleEntity>()
                .HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();

            modelBuilder.Entity<UserBookClubEntity>().HasKey(sc => new { sc.UserId, sc.BookClubId });

            modelBuilder.Entity<UserBookClubEntity>()
              .HasOne<UserEntity>(sc => sc.User)
              .WithMany(s => s.UserBookClubs)
              .HasForeignKey(sc => sc.UserId);


            modelBuilder.Entity<UserBookClubEntity>()
                .HasOne<BookClubEntity>(sc => sc.BookClub)
                .WithMany(s => s.UserBookClubs)
                .HasForeignKey(sc => sc.BookClubId);

            modelBuilder.Entity<UserEntity>()
                .HasOne(s => s.MyList)
                .WithOne(s => s.User);

            modelBuilder.Entity<MyBookEntity>()
           .HasOne<MyListEntity>(sc => sc.MyList)
           .WithMany(s => s.MyBooks)
           .HasForeignKey(sc => sc.MyListId);

            modelBuilder.Entity<MyMovieEntity>()
          .HasOne<MyListEntity>(sc => sc.MyList)
          .WithMany(s => s.MyMovies)
          .HasForeignKey(sc => sc.MyListId);

            modelBuilder.Entity<MySeriesEntity>()
          .HasOne<MyListEntity>(sc => sc.MyList)
          .WithMany(s => s.MySeries)
          .HasForeignKey(sc => sc.MyListId);
        }
    }
}
