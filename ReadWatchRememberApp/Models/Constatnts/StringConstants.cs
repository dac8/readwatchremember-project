﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Constatnts
{
    public static class StringConstants
    {
        public const string Message = "Message";
        public const string UserNotFound = "User not found";
        public const string MovieNotFound = "Movie not found";
        public const string BookNotFound = "Book not found";
        public const string SeriesNotFound = "Series not found";
        public const string BookClubNotFound = "Book club not found";
        public const string ListNotFound = "List not found";
        public const string AlreadyExists = "Already exists";
    }
}
