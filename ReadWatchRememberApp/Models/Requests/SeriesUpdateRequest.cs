﻿using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Requests
{
    public class SeriesUpdateRequest
    {
        public int Seasons { get; set; }
        public EnumSeriesStage Stage { get; set; }
    }
}
