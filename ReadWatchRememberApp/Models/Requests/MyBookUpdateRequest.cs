﻿using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Requests
{
    public class MyBookUpdateRequest
    {
       public int Rating { get; set; }
       public EnumBookStage Stage { get; set; }
    }
}
