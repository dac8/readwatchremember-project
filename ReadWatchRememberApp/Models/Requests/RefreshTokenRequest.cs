﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Requests
{
    public class RefreshTokenRequest
    {
        public string RefreshToken { get; set; }
    }
}
