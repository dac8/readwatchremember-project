﻿using ReadWatchRememberApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Requests
{
    public class MyMovieSeriesUpdateRequest
    {
        public int Rating { get; set; }
        public EnumMovieSeriesStage Stage { get; set; }
    }
}
