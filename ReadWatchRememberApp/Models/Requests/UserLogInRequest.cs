﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Models.Requests
{
    public class UserLogInRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
