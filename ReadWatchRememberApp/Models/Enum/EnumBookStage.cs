﻿namespace ReadWatchRememberApp.Models.Enum
{
    public enum EnumBookStage
    {
        Read,
        ToRead,
        CurrentlyReading
    }
}