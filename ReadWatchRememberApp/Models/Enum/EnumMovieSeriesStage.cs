﻿namespace ReadWatchRememberApp.Models.Enum
{
    public enum EnumMovieSeriesStage
    {
        Watched,
        ToWatch,
        CurrentlyWatching
    }
}
