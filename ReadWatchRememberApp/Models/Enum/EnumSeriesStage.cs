﻿namespace ReadWatchRememberApp.Models.Enum
{
    public enum EnumSeriesStage
    {
        OnAir,
        Cancelled,
        Ended
    }
}