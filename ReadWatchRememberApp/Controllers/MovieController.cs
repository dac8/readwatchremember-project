﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Controllers
{
    [ApiController]
    [Route("api/movie")]
    public class MovieController : ControllerBase
    {
        private readonly MovieService _entityService;

        public MovieController(MovieService bookListEntityService)
        {
            _entityService = bookListEntityService;
        }

        [HttpGet]
        public async Task<ObjectResult> GetAll()
        {
            return Ok(await _entityService.GetAll());
        }

        [HttpGet("{movieId}")]
        public async Task<ObjectResult> GetById([FromRoute] int movieId)
        {
            return Ok(await _entityService.GetById(movieId));
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{movieId}")]
        public async Task<ObjectResult> Delete([FromRoute] int movieId)
        {
            return Ok(await _entityService.Delete(await _entityService.GetById(movieId)));
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<ObjectResult> Update([FromBody] MovieEntity movie)
        {
            return Ok(await _entityService.Update(movie));
        }

        [Authorize(Roles ="Admin")]
        [HttpPost]
        public async Task<ObjectResult> CreateBook([FromBody] MovieEntity movie)
        {
            return Ok(await _entityService.Create(movie));
        }
    }
}
