﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadWatchRememberApp.Helpers;
using ReadWatchRememberApp.Models.Constatnts;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Requests;
using ReadWatchRememberApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Controllers
{
    [ApiController]
    [Route("api/myList")]
    public class MyListController : ControllerBase
    {
        private readonly MyListService _myListService;
        private readonly MyBookService _bookService;
        private readonly MyMovieService _movieService;
        private readonly MySeriesService _seriesService;
        private readonly IMapper _mapper;

        public MyListController(MyListService myListEntityService,
            MyBookService bookService,
            MyMovieService movieService,
            MySeriesService seriesService,
            IMapper mapper)
        {
            _myListService = myListEntityService;
            _bookService = bookService;
            _movieService = movieService;
            _seriesService = seriesService;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("GetAllMyLists")]
        public async Task<ObjectResult> GetAllMyLists()
        {
            return Ok(await _myListService.GetAll());
        }

        [HttpGet("myBook/GetAllResponse")]
        public async Task<ObjectResult> GetAllBooksResponse(int userId)
        {
            var list = await _myListService.GetUserList(userId);

            return Ok(await _bookService.GetAllResponse(list.Id));
        }

        [HttpGet("myBook/GetBookById/{bookId}")]
        public async Task<ObjectResult> GetBookById([FromRoute] int bookId)
        {
            return Ok(await _bookService.GetById(bookId));
        }

        [HttpDelete("myBook/DeleteMyBook/{bookId}")]
        public async Task<ObjectResult> DeleteMyBook([FromRoute] int bookId, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var result = await _bookService.GetAll(list.Id);

            return Ok(await _bookService.Delete(result.Where(p => p.Id == bookId).FirstOrDefault()));
        }

        [HttpPut("myBook/UpdateMyBook/{bookId}")]
        public async Task<ObjectResult> UpdateMyBook([FromRoute] int bookId, int userId, [FromBody] MyBookUpdateRequest book)
        {
            var list = await _myListService.GetUserList(userId);
            var result = await _bookService.GetAll(list.Id);

            return Ok(await _bookService.Update(_mapper.Map(book, result.Where(p => p.Id == bookId).FirstOrDefault())));
        }

        [HttpPost("myBook/CreateMyBook")]
        public async Task<ObjectResult> CreateMyBook([FromBody] MyBookEntity myBook, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var books = await _bookService.GetAll(list.Id);
            var result = books.Where(p => p.BookId == myBook.BookId).FirstOrDefault();

            myBook.MyListId = list.Id;
            myBook.CreatedBy = userId;
            myBook.ModifiedBy = userId;

            if (result == null)
            {
                return Ok(await _bookService.Create(myBook));
            }

            throw (new Exception(StringConstants.AlreadyExists));
        }


        [HttpDelete("myMovie/DeleteMyMovie/{movieId}")]
        public async Task<ObjectResult> DeleteMyMovie([FromRoute] int movieId, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var result = await _movieService.GetAll(list.Id);

            return Ok(await _movieService.Delete(result.Where(p => p.Id == movieId).FirstOrDefault()));
        }

        [HttpGet("myMovie/GetAllResponse")]
        public async Task<ObjectResult> GetAllMoviesResponse(int userId)
        {
            var list = await _myListService.GetUserList(userId);

            return Ok(await _movieService.GetAllResponse(list.Id));
        }

        [HttpGet("myMovie/GetMovieById/{movieId}")]
        public async Task<ObjectResult> GetMovieById([FromRoute] int movieId)
        {
            return Ok(await _movieService.GetById(movieId));
        }

        [HttpPut("myMovie/UpdateMyMovie/{movieId}")]
        public async Task<ObjectResult> UpdateMyMovie([FromRoute] int movieId, [FromBody] MyMovieSeriesUpdateRequest movie, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var result = await _movieService.GetAll(list.Id);

            return Ok(await _movieService.Update(_mapper.Map(movie, result.Where(p => p.Id == movieId).FirstOrDefault())));
        }

        [HttpPost("myMovie/CreateMyMovie")]
        public async Task<ObjectResult> CreateMyMovie([FromBody] MyMovieEntity myMovie, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var movies = await _movieService.GetAll(list.Id);
            var result = movies.Where(p => p.MovieId == myMovie.MovieId).FirstOrDefault();

            myMovie.MyListId = list.Id;
            myMovie.CreatedBy = userId;
            myMovie.ModifiedBy = userId;

            if (result == null)
            {
                return Ok(await _movieService.Create(myMovie));
            }

            throw (new Exception(StringConstants.AlreadyExists));
        }


        [HttpDelete("mySeries/DeleteMySeries/{seriesId}")]
        public async Task<ObjectResult> DeleteMySeries([FromRoute] int seriesId, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var result = await _seriesService.GetAll(list.Id);

            return Ok(await _seriesService.Delete(result.Where(p => p.Id == seriesId).FirstOrDefault()));
        }

        [HttpGet("mySeries/GetAllResponse")]
        public async Task<ObjectResult> GetAllSeriesResponse(int userId)
        {
            var list = await _myListService.GetUserList(userId);

            return Ok(await _seriesService.GetAllResponse(list.Id));
        }

        [HttpGet("mySeries/GetSeriesById/{seriesId}")]
        public async Task<ObjectResult> GetSeriesById([FromRoute] int seriesId)
        {
            return Ok(await _seriesService.GetById(seriesId));
        }

        [HttpPut("mySeries/UpdateMySeries/{seriesId}")]
        public async Task<ObjectResult> UpdateMySeries([FromRoute] int seriesId, [FromBody] MyMovieSeriesUpdateRequest series, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var result = await _seriesService.GetAll(list.Id);

            return Ok(await _seriesService.Update(_mapper.Map(series, result.Where(p => p.Id == seriesId).FirstOrDefault())));
        }

        [HttpPost("mySeries/CreateMySeries")]
        public async Task<ObjectResult> CreateMySeries([FromBody] MySeriesEntity mySeries, int userId)
        {
            var list = await _myListService.GetUserList(userId);
            var series = await _seriesService.GetAll(list.Id);
            var result = series.Where(p => p.SeriesId == mySeries.SeriesId).FirstOrDefault();

            mySeries.MyListId = list.Id;
            mySeries.CreatedBy = userId;
            mySeries.ModifiedBy = userId;

            if (result == null)
            {
                return Ok(await _seriesService.Create(mySeries));
            }

            throw (new Exception(StringConstants.AlreadyExists));
        }
    }
}
