﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Requests;
using ReadWatchRememberApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Controllers
{
    [ApiController]
    [Route("api/series")]
    public class SeriesController : ControllerBase
    {

        private readonly SeriesService _entityService;
        private readonly IMapper _mapper;

        public SeriesController(SeriesService bookListEntityService, IMapper mapper)
        {
            _entityService = bookListEntityService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ObjectResult> GetAll()
        {
            return Ok(await _entityService.GetAll());
        }

        [HttpGet("{seriesId}")]
        public async Task<ObjectResult> GetSeriesById([FromRoute] int seriesId)
        {
            return Ok(await _entityService.GetById(seriesId));
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{seriesId}")]
        public async Task<ObjectResult> Delete([FromRoute] int seriesId)
        {
            return Ok(await _entityService.Delete(await _entityService.GetById(seriesId)));
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{seriesId}")]
        public async Task<ObjectResult> Update([FromBody] SeriesUpdateRequest series, [FromRoute] int seriesId)
        {
            var result = await _entityService.GetById(seriesId);
            return Ok(await _entityService.Update(_mapper.Map(series,result)));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ObjectResult> CreateBook([FromBody] SeriesEntity series)
        {
            return Ok(await _entityService.Create(series));
        }
    }
}
