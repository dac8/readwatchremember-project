﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReadWatchRememberApp.Helpers;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Models.Requests;
using ReadWatchRememberApp.Services;
using SendGrid.Helpers.Errors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Controllers
{
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        private readonly BookClubService _bookClubService;
        private readonly ILogger _logger;

        public UserController(UserService userService, BookClubService bookClubService, ILoggerFactory logger )
        {
            _userService = userService;
            _bookClubService = bookClubService;
            _logger = logger.CreateLogger<UserController>();
        }

        [Authorize (Roles="Admin")]
        [HttpGet]
        public async Task<ObjectResult> GetUserDetails()
        {
            return Ok(await _userService.GetUserDetails(User.GetUserId()));
        }

        [HttpPost("register")]
        public async Task<ObjectResult> Register([FromBody] UserRegisterRequest userRequest,
             string role)
        {
            _logger.LogInformation("Registered");

            return Ok(await _userService.RegisterUser(userRequest, role));
        }

        [HttpPost("login")]
        public async Task<ObjectResult> Login([FromBody] UserLogInRequest userRequest)
        {
            _logger.LogInformation("Logged in");

            return Ok(await _userService.Login(userRequest.Username, userRequest.Password));
        }

        [HttpPut("token/refresh")]
        public async Task<ObjectResult> RefreshToken([FromBody] RefreshTokenRequest refreshTokenRequest)
        {
            return Ok(await _userService.RefreshToken(refreshTokenRequest.RefreshToken));
        }

        [HttpPut("token/revoke")]
        public async Task<ObjectResult> RevokeToken([FromBody] RefreshTokenRequest refreshTokenRequest)
        {
            return Ok(await _userService.RevokeRefreshToken(refreshTokenRequest.RefreshToken));
        }

        [HttpPost("register/resendEmail/{email}")]
        public async Task<ObjectResult> ResendEmail([FromRoute] string email)
        {
            return Ok(await _userService.ResendEmailConfirmation(email));
        }

        [HttpPut("register/confirmEmail")]
        public async Task<ObjectResult> ConfirmEmail([FromQuery] string email, [FromQuery] string code)
        {
            return Ok(await _userService.ConfirmEmail(email, code));
        }

        [HttpPut("bookClub/{bookClubId}")]
        public async Task<ObjectResult> JoinBookClub([FromRoute] int bookClubId, int userId)
        {
            var bookClub = await _bookClubService.GetById(bookClubId);
            var user = await _userService.GetById(userId);

            var userBookClub = new UserBookClubEntity()
            {
                User = user,
                BookClub = bookClub
            };

            bookClub.NrMembers++;
            bookClub.UserBookClubs.Add(userBookClub);
            user.UserBookClubs.Add(userBookClub);

            await _bookClubService.Update(bookClub);

            return Ok(await _userService.Update(user));

        }
    }
}
