﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Controllers
{
    [ApiController]
    [Route("api/book")]
    public class BookController : ControllerBase
    {
        private readonly BookService _entityService;

        public BookController(BookService bookListEntityService)
        {
            _entityService = bookListEntityService;
        }

        [HttpGet]
        public async Task<ObjectResult> GetAll()
        {
            return Ok(await _entityService.GetAll());
        }

        [HttpGet("{bookId}")]
        public async Task<ObjectResult> GetById([FromRoute] int bookId)
        {
            return Ok(await _entityService.GetById(bookId));
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{bookId}")]
        public async Task<ObjectResult> Delete([FromRoute] int bookId)
        {
            return Ok(await _entityService.Delete(await _entityService.GetById(bookId)));
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<ObjectResult> Update([FromBody] BookEntity book)
        {
            return Ok(await _entityService.Update(book));
        }

        [Authorize(Roles ="Admin")]
        [HttpPost]
        public async Task<ObjectResult> CreateBook([FromBody] BookEntity book)
        {
            return Ok(await _entityService.Create(book));
        }
    }
}
