﻿using Microsoft.AspNetCore.Mvc;
using ReadWatchRememberApp.Models.DataBase.Entities;
using ReadWatchRememberApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadWatchRememberApp.Controllers
{
    [ApiController]
    [Route("api/bookClub")]
    public class BookClubController : ControllerBase
    {
        private readonly BookClubService _entityService;

        public BookClubController(BookClubService bookClubEntityService)
        {
            _entityService = bookClubEntityService;
        }

        [HttpGet]
        public async Task<ObjectResult> GetAll()
        {
            return Ok(await _entityService.GetAll());
        }

        [HttpGet("{bookClubId}")]
        public async Task<ObjectResult> GetById([FromRoute] int bookClubId)
        {
            return Ok(await _entityService.GetById(bookClubId));
        }

        [HttpDelete("{bookClubId}")]
        public async Task<ObjectResult> Delete([FromRoute] int bookClubId)
        {
            return Ok(await _entityService.Delete(await _entityService.GetById(bookClubId)));
        }

        [HttpPut]
        public async Task<ObjectResult> Update([FromBody] BookClubEntity bookClub)
        {
            return Ok(await _entityService.Update(bookClub));
        }

        [HttpPost]
        public async Task<ObjectResult> CreateBook([FromBody] BookClubEntity bookClub)
        {
            return Ok(await _entityService.Create(bookClub));
        }
    }
}
